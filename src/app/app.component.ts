import { Component } from '@angular/core';

import { AppConfigService } from './app-config'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'empty';

  constructor(private appConfigService: AppConfigService) { }
  /**
  * Returns global notification options
  */
  public getNotificationOptions(): any {
    return this.appConfigService.get("notifications").options;
  }
}
