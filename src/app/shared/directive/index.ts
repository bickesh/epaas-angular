import { NgModule } from '@angular/core';
import { OnlyNumberDirective } from './only-number/only-number.directive';
import { OnlyAlphbatsDirective } from './only-alphabats/only-alphbats.directive';
import { OnlyAlphanumericsDirective } from './only-alphanumerics/only-alphanumerics.directive';

export const DIRECTIVE = [
  OnlyNumberDirective,
  OnlyAlphbatsDirective,
  OnlyAlphanumericsDirective
];

@NgModule({
  imports: [
  ],
  declarations: DIRECTIVE,
  exports: DIRECTIVE
})
export class DirectiveModule { }
