import { Injectable } from '@angular/core';
import { Sandbox } from '../../sandbox/base.sandbox';
import { Router } from '@angular/router';
@Injectable()
export class LayoutSandbox extends Sandbox {
  constructor(public router: Router) {
    super();
  }
}



