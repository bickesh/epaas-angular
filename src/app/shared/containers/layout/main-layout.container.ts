import { OnInit, Component } from '@angular/core';
import { LayoutSandbox } from './layout.sandbox';
import { Subscription } from "rxjs";


@Component({
  selector: 'main-layout',
  template: `
  <app-header-template></app-header-template>
  <ng-content></ng-content>  
  <app-footer-template></app-footer-template>
  `
})
export class MainLayoutContainer implements OnInit {

  private subscriptions: Array<Subscription> = [];
  constructor(
    public layoutSandbox: LayoutSandbox
  ) { }

  ngOnInit() {
    this.registerEvents();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  private registerEvents() {
  }
}