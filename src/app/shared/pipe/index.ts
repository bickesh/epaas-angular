import { NgModule } from '@angular/core';
import { EmailMaskingPipe } from './email-masking.pipe';
import { MobileNoMaskingPipe } from './mobileno-masking.pipe';
import { SeparatedDelimiterPipe } from './separated-delimiter';
export const PIPES = [
  EmailMaskingPipe,
  MobileNoMaskingPipe,
  SeparatedDelimiterPipe
];

@NgModule({
  imports: [],
  declarations: PIPES,
  exports: PIPES
})
export class PipesModule { }