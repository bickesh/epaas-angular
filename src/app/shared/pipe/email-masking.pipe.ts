import {
    Pipe,
    PipeTransform
} from '@angular/core';

@Pipe({
    name: 'emailMasking',
    pure: false
})
export class EmailMaskingPipe implements PipeTransform {

    constructor() { }

    transform(emailId: string): string {

        return emailId.replace(/(.{2})(.*)(?=@)/,
            //function (gp1, gp2, gp3) {
            function (gp2, gp3) {
                for (let i = 0; i < gp3.length; i++) {
                    gp2 += "*";
                } return gp2;
            });
    }
}