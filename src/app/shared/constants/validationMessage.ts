export const validationMessage={
    toasterClass:{
        success:'success',
        error:'error',
        warning:'warning',
    },
    common :{
        savedSuccessMsg:'Data Saved Successfully',
        savedFailedMsg:'Failed to save data',
        updateSuccessMsg:'Data Updated Successfully',
        updateFailedMsg:'Failed to update data',
        deleteSuccessMsg:'Data deleted Successfully',
        deleteFailedMsg:'Failed to delete data',
        minCharacterError: 'Minimum character required is ',
        maxCharacterError: 'Maximum character required is',
        minValueError: 'Minimum value is',
        maxValueError: 'Maximum value is',
        requireError: 'This is required',
        saveConfirmation:'There are some unsaved changes pending. Do you want to save?',
        deleteConfirmation: 'Record will be deleted, want to continue?',
        applicationError:'Unexpected error occured, Kindly contact administrator',
        formError: 'Please check all form fields',
        noFormChange:'No changes to save',
        dateError: 'Invalid Date',
        emailError : 'Email must be a valid email address',
        invalidData: 'Invalid value',
        noRecordsFound: 'No Records Found',
        dataAlreadyExist:'Data already exist',
        latitudeError:'Invalid latitude',
        longitudeError:'Invalid longitude',
        tokenInvalid:'Invalid token, redirecting to login',
        invalidUser:'Unathorise access, redirecting to login',
        forbiddenAccess:'Access forbidden, redirecting to login',
        requiredFileTypePDF:'Please upload PDF file only',
    },
    login:{
        usernameRequired:'Username is required',
        passwordRequired:'Password is required',
        passwordmatchErr:'password and confirm password should match',
        captchaRequire:'captcha not valid',
        otpRequire:'OTP is required',
        licenseRequired:'License number is required',
    },
    dashboard:{
        sameFromToDateError:'From date and To date must be same'
    }
  };
