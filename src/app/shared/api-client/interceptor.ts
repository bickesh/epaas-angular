﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
//import { HttpResponseHandler } from './httpResponseHandler.service';
import * as CryptoJS from 'crypto-js'




@Injectable()
export class HTTPStatus {
  constructor() {
  }
}

@Injectable()
export class HTTPListener implements HttpInterceptor {
  constructor(private spinner: NgxSpinnerService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let exceptEndpoints: Array<string> = [

    ];

    let reqUrl = "/" + this.getRelativeUrl(req.url);
    exceptEndpoints = exceptEndpoints.filter(endpoint => reqUrl.includes(endpoint));

    if (exceptEndpoints.length == 0) {
      req = req.clone({
        setHeaders: {
          'Content-Type': 'application/json'
        }
      });
    }

    if (sessionStorage.getItem('accessToken') != null && !reqUrl.includes('/serviceforfoscos') && !reqUrl.includes('/cbic/v1/getIECDetails')) {

      req = req.clone({
        setHeaders: {
          'Authorization': sessionStorage.getItem('accessToken') || ''
        }
      });

      if (sessionStorage.getItem('userSession')) {
        let userDetails = JSON.parse(sessionStorage.getItem('userSession') || '').userDetails;
        if (userDetails.userId != null) {
          req = req.clone({
            setHeaders: {
              'x-auth-user-id': this.EncrptText(userDetails.userId)
            }
          });
          if (sessionStorage.getItem("alternativeId")) {
            sessionStorage.removeItem("alternativeId");
          }
        } else {
          req = req.clone({
            setHeaders: {
              'x-auth-user-id': this.EncrptText(sessionStorage.getItem("alternativeId") || '')
            }
          });
        }
      }
    }

    this.spinner.show();
    return next.handle(req)
      .pipe(map(resp => {
        if (resp instanceof HttpResponse) {
          this.spinner.hide();
        }
        return resp;
      }));
  }


  /**
 * Returns relative url from the absolute path
 *
 * @param responseBody
 * @returns {string}
 */
  private getRelativeUrl(url: string): string {
    return url.toLowerCase().replace(/^(?:\/\/|[^\/]+)*\//, "");
  }

  EncrptText(plaintxt: string): string {
    let encrypted = CryptoJS.HmacSHA256(plaintxt, "LsiplyG3M1bX7Rg");
    return CryptoJS.enc.Base64.stringify(encrypted).toString();
  }
}
