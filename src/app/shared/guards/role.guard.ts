import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
  Route
} from "@angular/router";
import { Observable } from "rxjs";

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private _router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {

    let userRole = JSON.parse(sessionStorage.getItem("userSession") || '').roles[0];
    let roles: number[] = next.data.role;
    if (roles.includes(userRole.roleId)) {
      return true;
    }

    // navigate to not found page
    this._router.navigate(["/error"]);
    return false;
  }
}
