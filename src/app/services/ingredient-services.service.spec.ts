import { TestBed } from '@angular/core/testing';

import { IngredientServicesService } from './ingredient-services.service';

describe('IngredientServicesService', () => {
  let service: IngredientServicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IngredientServicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
